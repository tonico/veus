<p align="center"><a href="https://laravel.com" target="_blank">
    <img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable V++ersion"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Aplicação de teste para endereços de sites.

- usando SQLite e o banco esta em database/database.sqlite.
- usuário de teste: teste@teste.com - password: 123456
- a senha padrão de novos usuários: 123456

### Descrição

O font-end do cadastro é todo feito com Laravel/Bootstrap.
O back-end do cadastro é todo feito com Laravel/Bootstrap.

### Exemplo de arquivo .env

APP_NAME=THONICO

APP_ENV=production

APP_KEY=base64:vQdzuBL6qSP+BS8oberOyte0W3G8vWbP4hKPoSvdzt0=

APP_DEBUG=false

APP_URL=http://localhost

LOG_CHANNEL=stack

LOG_LEVEL=debug

DB_CONNECTION=sqlite

xDB_HOST=127.0.0.1

xDB_PORT=3306

xDB_DATABASE=daabase/database.sqlite

xDB_USERNAME=homestead

xDB_PASSWORD=secret

BROADCAST_DRIVER=log

CACHE_DRIVER=file

QUEUE_CONNECTION=sync

SESSION_DRIVER=file

SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1

REDIS_PASSWORD=null

REDIS_PORT=6379

MAIL_MAILER=smtp

MAIL_HOST=mailhog

MAIL_PORT=1025

MAIL_USERNAME=null

MAIL_PASSWORD=null

MAIL_ENCRYPTION=null

MAIL_FROM_ADDRESS=null

MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=

AWS_SECRET_ACCESS_KEY=

AWS_DEFAULT_REGION=us-east-1

AWS_BUCKET=

PUSHER_APP_ID=

PUSHER_APP_KEY=

PUSHER_APP_SECRET=

PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"

MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

xxJWT_SECRET=Rf0vjT4vzL13fucA6ppue3QIzgkjDKTJ2V85XabRjEyO2PV7coKaOk2TijXB1YW7

JWT_SECRET=thonico


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

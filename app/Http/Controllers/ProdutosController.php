<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProdutoRequest;
use App\Models\Produtos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;

class ProdutosController extends Controller
{

    /*
    public function __construct()
    {
        $this->middleware('auth', ['only' => 'api_produtos']);
    }
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registros = Produtos::orderBy('nome')->get();

        return view('produtos.index')
            ->with('registros', $registros)
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produtos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdutoRequest $request)
    {
        $input = $request->all();
        $reg = new Produtos();
        $reg->create($input);

        \Session::flash('message', trans( 'mensagens.conf_inc'));
        $url = asset('produtos');
        return redirect()->to($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reg = Produtos::find($id);
        return view('produtos.view')
            ->with('reg', $reg)
            ->with('view', true)
            ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reg = Produtos::find($id);
        return view('produtos.edit')
            ->with('reg', $reg)
            ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProdutoRequest $request, $id)
    {
        $input = $request->all();
        $reg = Produtos::find($id);

        $reg->update($input);

        \Session::flash('message', trans( 'mensagens.conf_alt'));
        $url = asset('produtos');
        return redirect()->to($url);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $registro = Produtos::find($id);
        $registro->delete();

        \Session::flash('message', trans( 'mensagens.conf_exc'));

        return redirect()->to(asset('produtos'));
    }

    public function api_produtos(Request $request) {

        $q = $request['q'];           //Input::get("q");

        $filtro = null;
        if ( isset($request['filter']))
            $filtro = $request['filter'];

        $sql_select = " select * ";
        $sql_from   = "   from produtos ";

        $sql_where  = "  where nome like '%" . $q . "%' ";

        if (is_null($filtro) == false) {
            $filtro = str_replace('brand:', '', $filtro);
            $sql_where .= " and marca = '" . $filtro . "' ";
        }

        $reg = DB::select( $sql_select . $sql_from . $sql_where );

        return response()->json($reg);
    }
}

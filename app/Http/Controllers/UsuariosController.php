<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registros = User::orderBy('name')->get();

        return view('usuarios.index')
            ->with('registros', $registros)
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt('123456');

        $reg = new User();
        $reg->create($input);

        \Session::flash('message', trans( 'mensagens.conf_inc'));
        $url = asset('usuarios');
        return redirect()->to($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reg = User::find($id);
        return view('usuarios.view')
            ->with('reg', $reg)
            ->with('view', true)
            ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reg = User::find($id);
        return view('usuarios.edit')
            ->with('reg', $reg)
            ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $reg = User::find($id);

        $reg->update($input);

        \Session::flash('message', trans( 'mensagens.conf_alt'));
        $url = asset('usuarios');
        return redirect()->to($url);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $registro = User::find($id);
        $registro->delete();

        \Session::flash('message', trans( 'mensagens.conf_exc'));

        return redirect()->to(asset('usuarios'));
    }
}

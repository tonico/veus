<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdutoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reg = $this->get('id');
        $id = $reg ? $reg : NULL;

        return [
            'nome'   => "required|min:3|unique:PRODUTOS,nome,$id,id",
        ];
    }

    public function messages()
    {
        return [
            'nome.required'     => trans('messages.crit_nome_required'),
            'nome.unique'       => trans('messages.crit_nome_unique'),
            'marca.required'  => trans('messages.crit_marca_required')
        ];
    }
}

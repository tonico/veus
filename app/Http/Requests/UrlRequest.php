<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UrlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reg = $this->get('id');
        $id = $reg ? $reg : NULL;

        return [
            'nome'  => "required|min:3|unique:url,nome,$id,id",
            'url'   => "required|min:3|unique:url,url,$id,id",
        ];
    }

    public function messages()
    {
        return [
            'nome.required'     => trans('mensagens.crit_nome_url_required'),
            'nome.unique'       => trans('mensagens.crit_nome_url_unique'),
            'url.required'     => trans('mensagens.crit_url_required'),
            'url.unique'       => trans('mensagens.crit_url_unique'),
        ];
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    use HasFactory;

    protected $table = 'url';
    protected $fillable = [
        'id',
        'url',
        'nome',
        'status',
        'retorno'
    ];
    protected $primaryKey = 'id';
    public $timestamps = true;

    public static function testarURLs() {
        $registros = Url::all();
        $conf = [];
        foreach ($registros as $r) {
            $ret = $r->testarURL($r->id);

            $conf[] = ['id' => $r->id, 'status' => $ret['status'], 'url' => $r->url];
        }

        // parametro de teste
        if (isset($_GET['teste'])==true) {
            var_dump($conf);
        }

        return http_response_code(200);
    }
}


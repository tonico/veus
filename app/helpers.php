<?php
/*
 * Created by PhpStorm.
 * User: Tonico
 * Date: 04/06/2016
 * Time: 20:45
 * Diversas Funcoes
 */

function is_valid_date($value, $format = 'dd.mm.yyyy'){
    Config::set('app.timezone', 'America/Sao_Paulo');
    if(strlen($value) >= 6 && strlen($format) == 10){

        // find separator. Remove all other characters from $format
        $separator_only = str_replace(array('m','d','y'),'', $format);
        $separator = $separator_only[0]; // separator is first character

        if($separator && strlen($separator_only) == 2){
            // make regex
            $regexp = str_replace('mm', '(0?[1-9]|1[0-2])', $format);
            $regexp = str_replace('dd', '(0?[1-9]|[1-2][0-9]|3[0-1])', $regexp);
            $regexp = str_replace('yyyy', '(19|20)?[0-9][0-9]', $regexp);
            $regexp = str_replace($separator, "\\" . $separator, $regexp);
            if($regexp != $value && preg_match('/'.$regexp.'\z/', $value)){

                // check date
                $arr=explode($separator,$value);
                $day=$arr[0];
                $month=$arr[1];
                $year=$arr[2];
                //if(@checkdate($month, $day, $year))
                if(@checkdate($day, $month, $year))
                    return true;
            }
        }
    }

    return false;
}

function data_to_sql( $data, $tipo = 1 ) {

    return $data;
    /*
    if ($data=='') {
        $resultado = null;
    }
    else {

        if ( strpos($data, '-') == 2 ) {
            $dia = substr($data, 0, 2);
            $mes = substr($data, 3, 2);
            $ano = substr($data, 5, 4);
        }
        else if ( strpos($data, '-') == 4 ) {
            $dia = substr($data, 8, 2);
            $mes = substr($data, 5, 2);
            $ano = substr($data, 0, 4);
        }
        else {
            $dia = substr($data, 0, 2);
            $mes = substr($data, 3, 2);
            $ano = substr($data, 6, 4);
        }
        //$resultado = "CONVERT( DATETIME, '" . $ano . $mes . $dia . "')";
        $resultado = "'" . $ano . "-" . $mes . "-" . $dia . "'";
    }
    */
    /*

if (($data=='') || ( is_null( $data ) )) {
    $resultado = null;
    return $resultado;
}
else {

    $resultado = $data;
    if ($tipo == 1)
        $resultado = Carbon::createFromFormat('d/m/Y', $data);

    if ($tipo == 2)
        $resultado = Carbon::createFromFormat('Y-m-d', $data);
    }
    //return $resultado->format('Ymd');
    */

    return $resultado;

}

function data_to_query( $data ) {
    //return $data;
    if ($data=='') {
        $resultado = null;
    }
    else {

        if ( strpos($data, '-') == 2 ) {
            $dia = substr($data, 0, 2);
            $mes = substr($data, 3, 2);
            $ano = substr($data, 5, 4);
        }
        else if ( strpos($data, '-') == 4 ) {
            $dia = substr($data, 8, 2);
            $mes = substr($data, 5, 2);
            $ano = substr($data, 0, 4);
        }
        else {
            $dia = substr($data, 0, 2);
            $mes = substr($data, 3, 2);
            $ano = substr($data, 6, 4);
        }
        //$resultado = "CONVERT( DATETIME, '" . $ano . $mes . $dia . "')";
        $resultado = "'" . $ano . "-" . $mes . "-" . $dia . "'";
    }
    return $resultado;
}

function data_to_obj( $data, $formato = 'd/m/Y') {
    $obj = \DateTime::createFromFormat($formato, $data);
    return $obj->format("m/d/Y");;
}

function hora_to_form( $hora ) {
    if ($hora=='') {
        $resultado = null;
    }
    else {
        if (strlen($hora) > 11) {
            $resultado = substr($hora, 11, 5);
        }
        else {
            $resultado = substr($hora, 0, 5);
        }
    }
    return $resultado;
}

function data_to_form( $data ) {
    if ($data=='') {
        $resultado = null;
    }
    else {
        if ( strpos($data, '-') == 2 ) {
            $dia = substr($data, 0, 2);
            $mes = substr($data, 3, 2);
            $ano = substr($data, 5, 4);
        }
        else if ( strpos($data, '-') == 4 ) {
            $dia = substr($data, 8, 2);
            $mes = substr($data, 5, 2);
            $ano = substr($data, 0, 4);
        }
        else {
            $dia = substr($data, 0, 2);
            $mes = substr($data, 3, 2);
            $ano = substr($data, 6, 4);
        }
        //$resultado = "CONVERT( DATETIME, '" . $ano . $mes . $dia . "')";
        $resultado = $ano . '-' . $mes . '-' . $dia;
    }
    return $resultado;
}

function hora_display( $data, $tipo)
{
    $data = str_replace("'", "", $data);
    $resultado = '';

    if ($data == '') {
        $resultado = '';
    }
    else {
        $hor = '';
        $min = '';
        $seg = '';

        if (strlen($data) >= 19) {
            $hor = substr($data, 11, 2);
            $min = substr($data, 14, 2);
            $seg = substr($data, 17, 2);
        }

        $resultado = $hor . ':' . $min;

        if ($tipo == 2)
            $resultado = $hor . ':' . $min . ':' . $seg;
    }
    return $resultado;
}


function data_display( $data ) {

    // retira o acento
    $data = str_replace( "'", "", $data);

    if ($data=='') {
        $resultado = '';
    }
    else {
        $dia = "00";
        $mes = "00";
        $ano = "0000";

        if ( strlen($data) >= 19 ) {
            //$mes = substr ($data, 1, 2);
            //$dia = substr ($data, 5, 2);
            //$ano = substr ($data, 8, 5);

            $mes = substr ($data, 5, 2);
            $dia = substr ($data, 8, 2);
            $ano = substr ($data, 0, 4);

            // define o m?s
            if ($mes == 'Jan') $mes = '01';
            if ($mes == 'Feb') $mes = '02';
            if ($mes == 'Mar') $mes = '03';
            if ($mes == 'Apr') $mes = '04';
            if ($mes == 'May') $mes = '05';
            if ($mes == 'Jun') $mes = '06';
            if ($mes == 'Jul') $mes = '07';
            if ($mes == 'Aug') $mes = '08';
            if ($mes == 'Sep') $mes = '09';
            if ($mes == 'Oct') $mes = '10';
            if ($mes == 'Nov') $mes = '11';
            if ($mes == 'Dec') $mes = '12';

            // retira os espa?os
            $dia = trim($dia);
            $ano = trim($ano);
            //$ano = $ano . ' - ' . $data;
        }
        else if ( (strlen($data) == 10) || (strlen($data) == 23)) {

            if ( strpos($data, '-') == 4 ) {
                $dia = substr($data, 8, 2);
                $mes = substr($data, 5, 2);
                $ano = substr($data, 0, 4);
            }

            if ( strpos($data, '-') == 2 ) {
                $dia = substr($data, 0, 2);
                $mes = substr($data, 3, 2);
                $ano = substr($data, 5, 4);
            }

            if ( strpos($data, '/') == 4 ) {
                $dia = substr($data, 8, 2);
                $mes = substr($data, 5, 2);
                $ano = substr($data, 0, 4);
            }

            if ( strpos($data, '/') == 2 ) {
                $dia = substr($data, 0, 2);
                $mes = substr($data, 3, 2);
                $ano = substr($data, 5, 4);
            }
        }
        else if (strlen($data) == 8) {
            $dia = substr($data, 6, 2);
            $mes = substr($data, 4, 2);
            $ano = substr($data, 0, 4);
        }
        else {
            $dia = $data;
            $mes = '';
            $ano = '';
        }

        $resultado = $dia . '/' . $mes . '/' . $ano;
    }
    return $resultado;
}

function time_display( $data ) {

    // retira o acento
    $data = str_replace( "'", "", $data);

    if ($data=='') {
        $resultado = '';
    }
    else {
        //$p = strpos(':', $data);
        $p = 11;

        $hor = substr( $data, $p, 2);
        $min = substr( $data, $p+3, 2);
        $seg = substr( $data, $p+6, 2);
        $resultado = $hor . ':' . $min . ':' . $seg;
    }
    return $resultado;
}

function PesquisaFK( $fk_name )
{
    $registros = DB::table('INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS')
        ->select(DB::raw('count(*) as fk'))
        ->where('constraint_name', $fk_name)
        ->get();

    $reg = $registros[0];

    //return dd($reg->fk);

    if ($reg->fk == "0") {
        return false;
    } else {
        return true;
    }
}

function DiaInicioSemana ($data)
{
    $dia_da_semana = jddayofweek($data);
    $resultado     = $data;

    if ($dia_da_semana == 0)
        $resultado = date_add($resultado, -6);
    if ($dia_da_semana == 2)
        $resultado = date_add($resultado, -1);
    if ($dia_da_semana == 3)
        $resultado = date_add($resultado, -2);
    if ($dia_da_semana == 4)
        $resultado = date_add($resultado, -3);
    if ($dia_da_semana == 5)
        $resultado = date_add($resultado, -4);
    if ($dia_da_semana == 6)
        $resultado = date_add($resultado, -5);

    return $resultado;
}

/* pega o valor de próximo codigo de algumas tabelas */
/**
 * @param $sNomeTabela
 * @return $id - o código da tabela ou nulo
 */
function BuscaProximoCodigo($sNomeTabela ) {
    $sql = "SELECT ISNULL( ULTIMO_ID, 1 ) AS ULTIMO_ID FROM AUTOINC WHERE AUTOINC.TABELA = '" . $sNomeTabela . "'";
    $qry = DB::select($sql);
    $id  = null;

    if (count($qry) > 0) {
        $id = $qry[0]->ULTIMO_ID + 1;

        // atualiza os codigo
        DB::table('AUTOINC')
            ->where('TABELA', $sNomeTabela)
            ->update(['ULTIMO_ID' => $id]);
    }
    return $id;
}

function ExibeRelacionamento($obj){

    if (isset($obj)) {
        $txt = $obj;
    }
    else {
        $txt = '';
    }
    return $txt;
}

function FormataNumeros( $num, $decimais = 0, $traco = true, $moeda = false){
    if ((is_null($num)) or ($num == 0)){
        $ret = 0;
        if ($traco) {
            $ret = "-";
        }
    }
    else {
        $ret = number_format( $num, $decimais, ',', '.');
        if ($moeda == true )
            $ret = trans('messages._moeda') . $ret;
    }
    return $ret;
}

function FormataNumerosSQL( $num ) {
    if ((is_null($num)) or ($num == 0) or ($num == '-')){
        $ret = 0;
    }
    else {
        $ret = str_replace('.', '' , $num );
        $ret = str_replace(',', '.', $ret );
    }
    return $ret;
}



// exibe o período de uma data
function periodo ( $data ){
    $dia = substr($data, 8, 2);
    $mes = substr($data, 5, 2);
    $ano = substr($data, 0, 4);

    return $mes . '/' . $ano;
}

function ano ($data){
    $ano = substr($data, 0, 4);
    return $ano;
}

function parametros( $chave, $default ){

    $_sql = "SELECT VALOR FROM PARAMETROS WHERE CAMPO = '" . $chave . "'";
    $reg = DB::select($_sql);

    $retorno = $reg[0]->VALOR;
    if ( is_null($retorno) ) {
        $retorno = $default;
    }

    return $retorno;
}

function diasemana($data){
    // Traz o dia da semana para qualquer data informada
    $dia =  substr($data,8,2);
    $mes =  substr($data,5,2);
    $ano =  substr($data,0,4);

    $diasemana = date("w", mktime(0,0,0,$mes,$dia,$ano) );

    switch($diasemana) {
        case "0": $diasemana = "Domingo";        break;
        case "1": $diasemana = "Segunda-Feira";  break;
        case "2": $diasemana = "Terça-Feira";    break;
        case "3": $diasemana = "Quarta-Feira";   break;
        case "4": $diasemana = "Quinta-Feira";   break;
        case "5": $diasemana = "Sexta-Feira";    break;
        case "6": $diasemana = "Sábado";		 break;
    }

    return $diasemana;
}


function validaHoras($campo){

    if ($campo =="") {
        return true;
    }

    if (preg_match('/^[0-9]{2}:[0-9]{2}$/', $campo)) {
        $horas = substr($campo, 0, 2);
        $minutos = substr($campo, 3, 2);
        //return dd($horas . '-' . $minutos);
        if (($horas > "23") OR ($minutos > "59")) {
            return false;
        }
        return true;
    }
    else {
        return false;
    }
}

function isValidUrl($url){
    // first do some quick sanity checks:
    if(!$url || !is_string($url)){
        return false;
    }
    // quick check url is roughly a valid http request: ( http://blah/... )
    if( ! preg_match('/^http(s)?:\/\/[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(\/.*)?$/i', $url) ){
        return false;
    }
    // the next bit could be slow:
    if(getHttpResponseCode_using_curl($url) != 200){
//      if(getHttpResponseCode_using_getheaders($url) != 200){  // use this one if you cant use curl
        return false;
    }
    // all good!
    return true;
}


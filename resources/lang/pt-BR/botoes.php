<?php
return [

    // imagens padrão
    'btn_alterar'           => 'fa fa-edit fa-2x',
    'btn_excluir'           => 'fa fa-trash fa-2x',
    'btn_imprimir'          => "fas fa-print fa-2x",
    'btn_incluir'           => 'fa fa-plus fa-2x',
    'btn_view'              => 'fa fa-search fa-2x',
    'btn_teste'             => 'fa fa-cloud fa-2x',
];

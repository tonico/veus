<?php
return [

    // mensagens padrão
    'versao'            => 'Versão 1.0.1',
    'welcome'           => 'Welcome to our application',
    '_moeda'            => 'R$ ',

    'alteracao'     => 'Alteração',
    'cancelar'      => 'Cancelar',
    'exclusao'      => 'Exclusão',
    'nao'           => 'Não',
    'pesquisa'      => 'Pesquisa',
    'salvar'        => 'Salvar',
    'sim'           => 'Sim',

    // mensagens
    'codigo'            => 'Código',
    'email'             => 'Email',
    'filtro'            => 'Filtro',
    'foto'              => 'Foto',
    'marca'             => 'Marca',
    'nome'              => 'Nome',
    'preco'             => 'Preço',
    'principal'         => 'Principal',
    'produtos'          => 'Produtos',
    'quantidade'        => 'Quantidade',
    'retorno'           => 'Retorno',
    'selecione_opcao'   => 'Selcione uma opção...',
    'sem_data'          => 'Sem Data',
    'status'            => 'Status',
    'testar'            => 'Testar',
    'usuarios'          => 'Usuários',
    'url'               => 'Url',
    'view'              => 'Consulta',

    // confirmações
    'conf_inc' => 'Inclusão efetuada com sucesso!',
    'conf_alt' => 'Alteração efetuada com sucesso!',
    'conf_exc' => 'Exclusão efetuada com sucesso!',

    // mensagens de exclusão
    'exc_produto'       => 'Deseja excluir este produto: ',

    // criticas
    'crit_nome_required' => 'É necessário informar o nome do produto.',
    'crit_nome_unique'   => 'Produto já cadastrado.',
    'crit_marca_required'=> 'É necessário informar a marca do produto.',

    'crit_nome_url_required' => 'É necessário informar o nome do site.',
    'crit_nome_url_unique'   => 'Site já cadastrado.',
    'crit_url_required' => 'É necessário informar a url.',
    'crit_url_unique'   => 'URL já cadastrada.',

    // log
    'log_login'   => 'Login',

    // erros
    'erro_autenticacao' => 'Falha na autenticação!',

    // mensagens do table
    "sLengthMenu"  => "Mostrar _MENU_ registros por página",
    "sZeroRecords" => "Nenhum registro encontrado",
    "sInfo"        => "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
    "sInfoEmpty"   => "Mostrando 0 / 0 de 0 registros",
    "sInfoFiltered"=> "(filtrado de _MAX_ registros)",
    "sSearch"      => "Pesquisar: ",
    "sFirst"       => "Início",
    "sPrevious"    => "Anterior",
    "sNext"        => "Próximo",
    "sLast"        => "Último",
    "sSelecione"   => "Selecione um registro"

];




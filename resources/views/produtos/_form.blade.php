    <div class="panel">
        <div class="panel-body">
            <div class="row form-group">
                <div class="col-8">
                    {!! Form::label('nome', trans('mensagens.nome')) !!}
                    {!! Form::text('nome', null, ['class'=>'form-control input-sm', 'id'=>'nome', 'maxlength'=>'50', 'required'] ) !!}
                </div>
                @if (isset($reg))
                <div class="col-1">
                    {!! Form::label('id', trans('mensagens.codigo')) !!}
                    {!! Form::text('id', null,['class'=>'form-control input-sm', 'id'=>'id', 'readonly']) !!}
                </div>
                @endif
            </div>

            <div class="row form-group">
                <div class="col-8">
                    {!! Form::label('marca', trans('mensagens.marca')) !!}
                    {!! Form::text('marca', null, ['class'=>'form-control input-sm', 'id'=>'marca', 'maxlength'=>'50', 'required'] ) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-4">
                    {!! Form::label('quantidade', trans('mensagens.quantidade')) !!}
                    {!! Form::text('quantidade', null, ['class'=>'form-control input-sm', 'id'=>'quantidade', 'maxlength'=>'6'] ) !!}
                </div>
                <div class="col-4">
                    {!! Form::label('preco', trans('mensagens.preco')) !!}
                    {!! Form::text('preco', null, ['class'=>'form-control input-sm', 'id'=>'preco'] ) !!}
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                @if( isset($view) == false)
                <div class="col-2">
                    {!! Form::submit(trans('mensagens.salvar')   , ['class'=>'btn btn-success pull-left']) !!}
                </div>
                <div class="col-2">
                    <a href="{{ asset('produtos')}}" class="btn btn-info pull-left">{!! trans('mensagens.cancelar') !!}</a>
                </div>
                @endif
            </div>
        </div>
    </div>
    <br>


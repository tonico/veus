@extends('adminlte::page')

@section('content_header')
    <h1 class="m-0 text-dark">{!! trans('mensagens.produtos') !!}</h1>
@stop

@section('content')
    {{ Form::open(['route'=>'produtos.store','method'=>'post', 'id'=>'form_', 'data-toggle'=>"validator", 'enctype'=>'multipart/form-data', 'role'=>"form"]) }}
    @include ('produtos._form')
    {{ Form::close() }}
@stop

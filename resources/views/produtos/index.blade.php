@extends('adminlte::page')

@section ( 'plugins.Datatables' , true )
@section ( 'plugins.Bootboxjs'  , true )
@section ( 'plugins.Sweetalert2', true )

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>{!! trans('mensagens.produtos') !!}</h4>
        </div>
        <div class="box-body">
            <table class='table table-striped' id="tbl_">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{!! trans("mensagens.nome") !!}</th>
                    <th>{!! trans("mensagens.marca") !!}</th>
                    <th>{!! trans("mensagens.quantidade") !!}</th>
                    <th>{!! trans("mensagens.preco") !!}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($registros as $reg)
                    <tr>
                        {!! Form::open(['route' => [ 'produtos.destroy', 'produto' => $reg->id]
                                , 'method' =>'DELETE'
                                , 'id' => "delete-form-{$reg->id}"
                                , 'style' => 'display:none'
                                ]) !!}
                        {!! Form::close() !!}
                        <td>{{$reg->id}}</td>
                        <td>{{$reg->nome}}</td>
                        <td>{{$reg->marca}}</td>
                        <td>{{$reg->quantidade}}</td>
                        <td>{{$reg->preco}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section("js")
    <script>
        jQuery.extend( jQuery.fn.dataTableExt.oSort, {
            "date-br-pre": function ( a ) {
                if (a == null || a == "") {
                    return 0;
                }
                var brDatea = a.split('/');
                return (brDatea[2] + brDatea[1] + brDatea[0]) * 1;
            },

            "date-br-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "date-br-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        } );

        $(document).ready(function () {
            $('#tbl_').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "order": [[ 1, 'asc' ]],
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "select": true,
                "columnDefs": [
                ],
                dom: 'Bfrtip',
                buttons: [
                    {
                        "className": "{!! trans('botoes.btn_incluir')!!}",
                        "titleAttr": "{!! trans('mensagens.inclusao')!!}",
                        "action": function (e, dt, node, config) {
                            location.href = "{!! asset('produtos/create') !!}";
                        }
                    },
                    {
                        "className": "{!! trans('botoes.btn_alterar')!!}",
                        "titleAttr": "{!! trans('mensagens.alteracao')!!}",
                        "action": function (e, dt, node, config) {
                            //dados = $('#tbl_').row('.selected').data();
                            dados = $('#tbl_').DataTable().row('.selected').data();
                            if (dados == null) {
                                Swal.fire("{!! trans('mensagens.sSelecione') !!}");
                                //bootbox.alert("{!! trans('mensagens.sSelecione') !!}");
                            }
                            else {
                                // pega o código
                                id = dados[0];
                                url = '{{ asset('produtos')  }}/' + id + '/edit';
                                location.href = url;
                            }
                        }
                    },
                    {
                        "className": "{!! trans('botoes.btn_excluir')!!}",
                        "titleAttr": "{!! trans('mensagens.exclusao')!!}",
                        "action": function (e, dt, node, config) {
                            dados = $('#tbl_').DataTable().row('.selected').data();
                            if (dados == null) {
                                Swal.fire("{!! trans('mensagens.sSelecione') !!}", 'success');
                                //bootbox.alert("{!! trans('mensagens.sSelecione') !!}");
                            } else {
                                var _descr = dados[1];
                                var _id = dados[0];
                                var _nome = '#delete-form-' + _id;
                                //alert(_nome);

                                Swal.fire({
                                    title: "{!! trans('mensagens.exclusao') !!}",
                                    text: "{!! trans('mensagens.exc_produto') !!}" + _descr + "?",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    cancelButtonText: '{!! trans("mensagens.nao") !!}',
                                    confirmButtonText: '{!! trans("mensagens.sim") !!}'
                                }).then((result) => {
                                    if (result.value == true) {
                                        $(_nome).submit();
                                    }
                                })
                            }
                        }
                    },
                    {
                        "className": "{!! trans('botoes.btn_view')!!}",
                        "titleAttr": "{!! trans('mensagens.view')!!}",
                        "action": function (e, dt, node, config) {
                            dados = $('#tbl_').DataTable().row('.selected').data();
                            if (dados == null) {
                                Swal.fire("{!! trans('mensagens.sSelecione') !!}", 'success' );
                                //bootbox.alert("{!! trans('mensagens.sSelecione') !!}");
                            }
                            else {
                                var _descr = dados[1];
                                var _id = dados[0];

                                id = dados[0];
                                url = '{{ asset('produtos')  }}/' + id;
                                location.href = url;
                            }
                        }
                    }
                ],
                "language": {
                    "sLengthMenu": "{!!  trans('mensagens.sLengthMenu') !!}",
                    "sZeroRecords": "{!!  trans('mensagens.sZeroRecords') !!}",
                    "sInfo": "{!!  trans('mensagens.sInfo') !!}",
                    "sInfoEmpty": "{!!  trans('mensagens.sInfoEmpty') !!}",
                    "sInfoFiltered": "{!!  trans('mensagens.sInfoFiltered') !!}",
                    "sSearch": "",
                    "oPaginate": {
                        "sFirst": "{!!  trans('mensagens.sFirst') !!}",
                        "sPrevious": "{!!  trans('mensagens.sPrevious') !!}",
                        "sNext": "{!!  trans('mensagens.sNext') !!}",
                        "sLast": "{!!  trans('mensagens.sLast') !!}"
                    }
                }
            });

            $('.dataTables_filter input').addClass('form-control pull-right');
            $('.dataTables_filter input').attr('placeholder', "{!! trans('mensagens.pesquisa') !!}");
            $('.dataTables_filter').addClass('pull-right');
        });
    </script>
@endsection

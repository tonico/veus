    <div class="panel">
        <div class="panel-body">
            @extends('flash-message')
            <div class="row form-group">
                <div class="col-8">
                    {!! Form::label('nome', trans('mensagens.nome')) !!}
                    {!! Form::text('nome', null, ['class'=>'form-control input-sm', 'id'=>'nome', 'maxlength'=>'255', 'required'] ) !!}
                </div>
                @if (isset($reg))
                <div class="col-1">
                    {!! Form::label('id', trans('mensagens.codigo')) !!}
                    {!! Form::text('id', null,['class'=>'form-control input-sm', 'id'=>'id', 'readonly']) !!}
                </div>
                @endif
            </div>

            <div class="row form-group">
                <div class="col-8">
                    {!! Form::label('url', trans('mensagens.url')) !!}
                    {!! Form::text('url', null, ['class'=>'form-control input-sm', 'id'=>'url', 'maxlength'=>'255', 'required'] ) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-2">
                    {!! Form::label('status', trans('mensagens.status')) !!}
                    {!! Form::text('status', null, ['class'=>'form-control input-sm', 'id'=>'status', 'readonly'=>'true'] ) !!}
                </div>
                <div class="col-6">
                    {!! Form::label('retorno', trans('mensagens.retorno')) !!}
                    {!! Form::text('retorno', null, ['class'=>'form-control input-sm', 'id'=>'retorno', 'readonly'=>'true'] ) !!}
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                @if( isset($view) == false)
                    <div class="col-2">
                        {!! Form::submit(trans('mensagens.salvar')   , ['class'=>'btn btn-success pull-left']) !!}
                    </div>
                    <div class="col-2">
                        <a href="{{ asset('url')}}" class="btn btn-info pull-left">{!! trans('mensagens.cancelar') !!}</a>
                    </div>
                    @if(isset($reg))
                        <div class="col-2">
                            <a href="" class="btn btn-danger pull-left">{!! trans('mensagens.testar') !!}</a>
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
    <br>

@section('js')
<script>

</script>
@endsection

@extends('adminlte::page')

@section('content_header')
    <h1 class="m-0 text-dark">{!! trans('mensagens.url') !!}</h1>
@stop

@section('content')
    {{ Form::open(['route'=>'url.store','method'=>'post', 'id'=>'form_', 'data-toggle'=>"validator", 'enctype'=>'multipart/form-data', 'role'=>"form"]) }}
    @include ('url._form')
    {{ Form::close() }}
@stop

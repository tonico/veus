@extends('adminlte::page')

@section('content_header')
    <h1 class="m-0 text-dark">{!! trans('mensagens.url') !!}</h1>
@stop


@section('content')
    {!! Form::model($reg,[ 'route'=>['url.update', $reg->id], 'method'=>'PUT', 'id'=>'form_', 'enctype'=>'multipart/form-data', 'file'=>true]) !!}
    <fieldset disabled>
    @include ('url._form')
    </fieldset>
    <div class="row">
        <div class="col-2">
            <a href="{{ asset('url')}}" class="btn btn-info pull-left">{!! trans('mensagens.cancelar') !!}</a>
        </div>
    </div>

    {!! Form::close() !!}
@stop

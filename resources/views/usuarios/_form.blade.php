    <div class="panel">
        <div class="panel-body">
            <div class="row form-group">
                <div class="col-8">
                    {!! Form::label('name', trans('mensagens.nome')) !!}
                    {!! Form::text('name', null, ['class'=>'form-control input-sm', 'id'=>'name', 'maxlength'=>'100', 'required'] ) !!}
                </div>
                @if (isset($reg))
                <div class="col-1">
                    {!! Form::label('id', trans('mensagens.codigo')) !!}
                    {!! Form::text('id', null,['class'=>'form-control input-sm', 'id'=>'id', 'readonly']) !!}
                </div>
                @endif
            </div>

            <div class="row form-group">
                <div class="col-8">
                    {!! Form::label('email', trans('mensagens.email')) !!}
                    {!! Form::text('email', null, ['class'=>'form-control input-sm', 'id'=>'email', 'maxlength'=>'100', 'required'] ) !!}
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                @if( isset($view) == false)
                <div class="col-2">
                    {!! Form::submit(trans('mensagens.salvar')   , ['class'=>'btn btn-success pull-left']) !!}
                </div>
                <div class="col-2">
                    <a href="{{ asset('usuarios')}}" class="btn btn-info pull-left">{!! trans('mensagens.cancelar') !!}</a>
                </div>
                @endif
            </div>
        </div>
    </div>
    <br>


@extends('adminlte::page')
@section('content_header')
    <h1 class="m-0 text-dark">{!! trans('mensagens.produtos') !!}</h1>
@stop


@section('content')
    {!! Form::model($reg,[ 'route'=>['usuarios.update', $reg->id], 'method'=>'PUT', 'id'=>'form_', 'enctype'=>'multipart/form-data', 'file'=>true]) !!}
    @include ('usuarios._form')
    {!! Form::close() !!}
@stop

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
    //return view('/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home', function() {
    return view('home');
})->name('home')->middleware('auth');

Route::resource( 'usuarios', 'App\Http\Controllers\UsuariosController')->middleware('auth');

Route::resource( 'url', 'App\Http\Controllers\UrlController')->middleware('auth');
Route::get('url/testarUrl/{id}' , 'App\Http\Controllers\UrlController@testarURL')->middleware('auth');
Route::get('testarUrls'     , 'App\Http\Controllers\UrlController@testarURLs')->middleware('auth');
